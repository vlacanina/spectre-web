package es.dospa.spectre.spectreweb;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpectreWebApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpectreWebApplication.class, args);
	}

}
